import Vue from 'vue'
import App from './App.vue'
import 'mint-ui/lib/style.css'
import  MintUI from 'mint-ui'
import router from './router'
import axios from 'axios'
import store from './store'
// 引入echarts
import echarts from 'echarts'

Vue.prototype.$echarts = echarts;
Vue.prototype.$http= axios;

Vue.use(MintUI);
Vue.config.productionTip = false;

// require('../statics/css/common.css')
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

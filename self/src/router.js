import Vue from 'vue'

import Router from 'vue-router'
import Home from './views/Home.vue'
import Self from './views/self.vue'
import Setting from './views/setting.vue'

import Overview from './views/group/overview.vue'
import Region from './views/group/region.vue'
import Community from './views/group/community.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
        path: '/',
        name: 'home',
        component: Home,
        children:[
            {
                path: '/home',
                name: 'home',
                component: Home
            }
        ]
    },
    {
        path: '/self',
        name: 'self',
        component: Self
    },
    {
        path: '/setting',
        name: 'setting',
        component: Setting
    },
    {
        path: '/group/overview',
        name: 'overview',
        component: Overview
    },
    {
        path: '/group/region',
        name: 'region',
        component: Region
    },
    {
      path: '/group/community',
      name: 'community',
      component: Community
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
